from django.db import models

# Create your models here.
class Vehiculo(models.Model):
    idvehiculo = models.AutoField(primary_key= True)
    idtipo = models.IntegerField()
    marca = models.CharField(max_length=100, null=False, blank=False)
    anio = models.CharField(max_length=100)
    soat = models.DateField()
    tecnicomecanica = models.DateField()
    estado = models.CharField(max_length=100)
    fecha_actualizacion = models.DateField()

class tipo_vehiculo(models.Model):
    idtipo = models.AutoField(primary_key= True)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.marca