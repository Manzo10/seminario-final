from django.shortcuts import render
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .serializers import VehiculoSerializers
from .models import Vehiculo
# Create your views here.

# @api_view(['GET'])
# def apiOverview(request):
#     api_urls = {
#         'List': '/vehiculo-list',
#         'Detail View': '/vehiculo-detail/<int:id>',
#         'Create': '/vehiculo-create',
#         'update': '/vehiculo-update/<int:id>',
#         'Delete': '/vehiculo-detail/<int:id>',
#     }

#     return Response(api_urls)

@api_view(['GET'])
def showAll(request):
    vehiculos = Vehiculo.objects.all()
    serializers = VehiculoSerializers(vehiculos, many= True)
    return Response(serializers.data)

@api_view(['GET'])
def viewVehiculo(request, pk):
    vehiculo = Vehiculo.objects.get(id=pk)
    serializers = VehiculoSerializers(vehiculo, many= False)
    return Response(serializers.data)

@api_view(['POST'])
def createVehiculo(request):
    
    serializers = VehiculoSerializers(data= request.data)
    if serializers.is_valid():
        serializers.save()
    return Response(serializers.data)


@api_view(['POST'])
def updateVehiculo(request):
    vehi = Vehiculo.objects.get(id=pk)
    serializers = VehiculoSerializers(instance=vehi, data=request.data)
    if serializers.is_valid():
        serializers.save()

    return Response(serializers.data)


@api_view(['GET'])
def deleteVehiculo(request, pk):
    vehiculo = Vehiculo.objects.get(id=pk)
    vehiculo.delete()

    return Response("eliminado")


