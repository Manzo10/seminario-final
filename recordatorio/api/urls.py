from django.urls import path
from . import views

urlpatterns=[
    # path('', views.apiOverview, name='apiOverview'),
    path('vehiculo-list', views.showAll, name='vehiculo-list'),
    path('vehiculo-detail/<int:pk>', views.viewVehiculo, name='vehiculo-detail'),
    path('vehiculo-create/', views.createVehiculo, name='vehiculo-create'),
    path('vehiculo-update/<int:pk>/', views.updateVehiculo, name='vehiculo-update'),
    path('vehiculo-delete/<int:pk>/', views.deleteVehiculo, name='vehiculo-delete'),


]